package me.test;

import jdk.nashorn.api.scripting.NashornScriptEngineFactory;

import java.lang.System;

public class Main {

    public static void main(String[] args) {
        NashornScriptEngineFactory factory = new NashornScriptEngineFactory();
        factory.getScriptEngine("-d=somedir");
        factory.getScriptEngine();
    }
}
